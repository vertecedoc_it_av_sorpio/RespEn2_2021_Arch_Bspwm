# Defined in - @ line 1
function yas --wraps='yay -S' --wraps='yay -S --noconfirm' --description 'alias yas yay -S --noconfirm'
  yay -S --noconfirm $argv;
end
